import React from 'react'
import Footer from './Footer'
import Admission from './Admission'
import Navbar from './NavBar'

const Admission1 = () => {
    return (
        <div>
            <Navbar />
            <Admission title="CONTACT" />
            <Footer />
        </div>
    )
}

export default Admission1
